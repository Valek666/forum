﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Models
{
    public class Message
    {
        public int Id { get; set; }
        public string DescriptionMessage { get; set; }
        public DateTime DateTimeMessage { get; set; }
        public int LikeCount { get; set; }
        public int DiseLikeCount { get; set; }

        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        [ForeignKey("Theme")]
        public int ThemeId { get; set; }
        public Theme Theme { get; set; }
    }
}
/*содержимое
автор
дата
лайки
дизлайки*/

