﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forum.Models;

namespace Forum.IndexViewModel
{
    public class IndexViewModelPage
    {
        public PageViewModel PageViewModel { get; set; }
        public List<Message> Messages { get; internal set; }
    }
}
