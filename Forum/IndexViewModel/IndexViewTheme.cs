﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forum.Models;

namespace Forum.IndexViewModel
{
    public class IndexViewTheme
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Message> Messages { get; set; }
    }
}
